Hi there! I wanted to go above and beyond with my background music,
so I found the same track in three different tempos (speeds). I 
have a script (AudioController.cs) that changes the file based on
the different Ghost states, so the music sounds continuous, just
slightly faster or slower. Dr Raffe confirmed this was ok as it is
still 3 files. :)