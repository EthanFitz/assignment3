using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
  public GameObject pellet;
  public GameObject powerPellet;
  public GameObject wall;
  public Sprite[] wallSprites;

  int[,] levelMap = {
    {1,2,2,2,2,2,2,2,2,2,2,2,2,7},
    {2,5,5,5,5,5,5,5,5,5,5,5,5,4},
    {2,5,3,4,4,3,5,3,4,4,4,3,5,4},
    {2,6,4,0,0,4,5,4,0,0,0,4,5,4},
    {2,5,3,4,4,3,5,3,4,4,4,3,5,3},
    {2,5,5,5,5,5,5,5,5,5,5,5,5,5},
    {2,5,3,4,4,3,5,3,3,5,3,4,4,4},
    {2,5,3,4,4,3,5,4,4,5,3,4,4,3},
    {2,5,5,5,5,5,5,4,4,5,5,5,5,4},
    {1,2,2,2,2,1,5,4,3,4,4,3,0,4},
    {0,0,0,0,0,2,5,4,3,4,4,3,0,3},
    {0,0,0,0,0,2,5,4,4,0,0,0,0,0},
    {0,0,0,0,0,2,5,4,4,0,3,4,4,0},
    {2,2,2,2,2,1,5,3,3,0,4,0,0,0},
    {0,0,0,0,0,0,5,0,0,0,4,0,0,0},
  };
  int[,] rotationMap = {
    {0,1,1,1,1,1,1,1,1,1,1,1,1,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,1,1,1,0,0,1,1,1,1,0,0},
    {0,0,0,0,0,2,0,0,0,0,0,2,0,0},
    {0,0,3,3,3,2,0,3,3,3,3,2,0,3},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0},
    {0,0,0,1,1,1,0,0,1,0,0,1,1,1},
    {0,0,3,3,3,2,0,0,2,0,3,3,3,1},
    {0,0,0,0,0,0,0,0,2,0,0,0,0,0},
    {3,3,3,3,3,1,0,0,3,1,1,1,0,0},
    {0,0,0,0,0,0,0,0,0,3,3,2,0,3},
    {0,0,0,0,0,0,0,0,2,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,2,0,0,1,1,0},
    {1,1,1,1,1,2,0,3,2,0,0,0,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,0},
  };

  void Start(){
    GenerateLevel();
  }

  void GenerateLevel(){ //q stands for quadrant, r and c stand for row and column, respectively
    for (int q = 1; q < 5; q++){ //1 = top-left, 2 = bottom-left, 3 = top-right, 4 = bottom-right
      int limit = GetLimit(q);
      for (int r = 0; r < limit; r++){ //15's hard-coded in because I kept getting out of bounds errors with .Length
        for (int c = 0; c < 14; c++){ //couldn't work out how to get the width of the 2D array, so it's hard-coded in
          Vector3 position = GetPosition(q, r, c);
          int rotation = GetRotation(q, rotationMap[r,c]);
          Vector3 scaleModifier = GetScaleModifier(q);
          if (levelMap[r,c] == 1){ //outside corner
            GameObject newWall = Instantiate(wall, position, Quaternion.Euler(0f, 0f, rotation));
            newWall.transform.localScale = Vector3.Scale(newWall.transform.localScale, scaleModifier);
            newWall.GetComponent<SpriteRenderer>().sprite = wallSprites[0];
            newWall.SetActive(true);
          } else if (levelMap[r,c] == 2){ //outside wall
            GameObject newWall = Instantiate(wall, position, Quaternion.Euler(0f, 0f, rotation));
            newWall.transform.localScale = Vector3.Scale(newWall.transform.localScale, scaleModifier);
            newWall.GetComponent<SpriteRenderer>().sprite = wallSprites[2];
            newWall.SetActive(true);
          } else if (levelMap[r,c] == 3){ //inside corner
            GameObject newWall = Instantiate(wall, position, Quaternion.Euler(0f, 0f, rotation));
            newWall.transform.localScale = Vector3.Scale(newWall.transform.localScale, scaleModifier);
            newWall.GetComponent<SpriteRenderer>().sprite = wallSprites[4];
            newWall.SetActive(true);
          } else if (levelMap[r,c] == 4){ //inside wall
            GameObject newWall = Instantiate(wall, position, Quaternion.Euler(0f, 0f, rotation));
            newWall.transform.localScale = Vector3.Scale(newWall.transform.localScale, scaleModifier);
            newWall.GetComponent<SpriteRenderer>().sprite = wallSprites[3];
            newWall.SetActive(true);
          } else if (levelMap[r,c] == 5){ //pellet
            GameObject newPellet = Instantiate(pellet, position, Quaternion.identity);
            newPellet.SetActive(true);
          } else if (levelMap[r,c] == 6){ //power  pellet
            GameObject newPowerPellet = Instantiate(powerPellet, position, Quaternion.identity);
            newPowerPellet.SetActive(true);
          } else if (levelMap[r,c] == 7){ //junction wall
            GameObject newWall = Instantiate(wall, position, Quaternion.Euler(0f, 0f, rotation));
            newWall.transform.localScale = Vector3.Scale(newWall.transform.localScale, scaleModifier);
            newWall.GetComponent<SpriteRenderer>().sprite = wallSprites[1];
            newWall.SetActive(true);
          }
        }
      }
    }
  }

  int GetLimit(int quadrant){
    if (IsEven(quadrant)){
      return 14;
    } else {
      return 15;
    }
  }

  Vector3 GetPosition(int quadrant, int row, int column){
    if (IsEven(quadrant)){
      row -= 14;
    } else {
      row = (row * -1) + 14;
    }

    if (quadrant < 3){
      column -= 14;
    } else {
      column = (column * -1) + 13;
    }

    return new Vector3(column, row, 0f);
  }

  int GetRotation(int quadrant, int value){
    if (quadrant == 1 || quadrant == 4){
      return -90 * value;
    } else {
      return 90 * value;
    }
  }

  Vector3 GetScaleModifier(int quadrant){
    int x = 1;
    int y = 1;

    if (IsEven(quadrant)){
      y = -1;
    }

    if (quadrant > 2){
      x = -1;
    }

    return new Vector3(x, y, 1f);
  }

  bool IsEven(int number){
    return (number % 2 == 0);
  }
}
