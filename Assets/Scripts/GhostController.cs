using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostController : MonoBehaviour
{
  public GameObject audioOutput;

  int idleCounter = 0;
  int downCounter = 0;
  Animator animator;

  void Start(){
    animator = this.GetComponent<Animator>();
  }

  void OnIdleStart(){
    audioOutput.GetComponent<AudioController>().ChangeTempo(0);
  }

  void OnIdleComplete(){
    idleCounter++;
    if (idleCounter >= 6){
      InvertScale();
      idleCounter = 0;
    }
  }

  void OnDownComplete(){
    downCounter++;
    if (downCounter >= 3){
      InvertScale();
      downCounter = 0;
    }
  }

  void OnScaredStart(){
    audioOutput.GetComponent<AudioController>().ChangeTempo(1);
  }

  void OnDeadStart(){
    audioOutput.GetComponent<AudioController>().ChangeTempo(2);
  }

  void OnDeadComplete(){
    animator.SetTrigger("GoDown");
    animator.SetTrigger("GoUp");
    animator.SetTrigger("GoScared");
    animator.SetTrigger("GoRecovering");
    animator.SetTrigger("GoDead");
    idleCounter = 0;
  }

  void InvertScale(){
    transform.localScale = new Vector3(transform.localScale.x * -1f, transform.localScale.y, transform.localScale.z);
  }
}
